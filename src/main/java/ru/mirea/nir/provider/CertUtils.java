package ru.mirea.nir.provider;

import javafx.util.Pair;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.*;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemWriter;
import ru.mirea.nir.exceptions.*;
import ru.mirea.nir.exceptions.PasswordException;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Enumeration;
import java.util.Map;


public class CertUtils {
    public static void writeX509Certificate(X509Certificate certificate, String certPath) {
        try {
            PemWriter writer = new PEMWriter(new FileWriter(new File(certPath + "\\cert.crt")));
            PemObject obj = new PemObject("CERTIFICATE", certificate.getEncoded());
            writer.writeObject(obj);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    public static void writeX509CertificateAndKey(X509Certificate certificate, PrivateKey privateKey, String certPath) {
        try{
            writeX509Certificate(certificate, certPath);
            PemWriter writer = new PEMWriter(new FileWriter(new File(certPath + "\\cert.key")));
            PemObject obj = new PemObject("EC PRIVATE KEY", privateKey.getEncoded());
            writer.writeObject(obj);
            writer.flush();
            writer.close();
        }catch (Exception e){
            throw new CertificateException(e);
        }
    }

    public static void writePKCS12Certificate(Certificate certificate, PrivateKey privateKey, String certPath, char[] password) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            KeyStore ks = KeyStore.getInstance("PKCS12", "BC");
            ks.load(null);

            ks.setCertificateEntry("certAlias", certificate);
            ks.setKeyEntry("privateKeyAlias", privateKey, password, new Certificate[]{certificate});
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ks.store(out, password);
            out.close();

            FileOutputStream os = new FileOutputStream(new File(certPath + "\\cert.pfx"));
            os.write(out.toByteArray());
            os.close();
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    public static X509Certificate readX509Cert(String certPath) {
        try{
            Security.addProvider(new BouncyCastleProvider());
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            FileInputStream fin = new FileInputStream(certPath);
            X509Certificate cert = (X509Certificate) cf.generateCertificate(fin);
            if (cert == null) {
                throw new CertificateException("Невозможно прочитать сертификат");
            }
            return cert;
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    public static PublicKey readPublicKeyPEM(String keyPath) {
        X509Certificate cert = readX509Cert(keyPath);
        if (cert == null) {
            throw new CertificateException("Невозможно прочитать сертификат");
        }
        if (cert.getPublicKey() != null) {
            return cert.getPublicKey();
        } else {
            throw new CertificateException("Невозможно получить ключ из сертификата");
        }
    }

    public static PrivateKey readPrivateKeyPEM(String keyPath) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            KeyFactory kf = KeyFactory.getInstance("EC", "BC");
            PemReader reader = new PemReader(new FileReader(new File(keyPath)));
            PemObject pemObject = reader.readPemObject();
            byte[] content = pemObject.getContent();
            reader.close();

            FileInputStream fin = new FileInputStream(keyPath);
            EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(content);
            return kf.generatePrivate(keySpec);
        } catch (IOException | NoSuchProviderException | InvalidKeySpecException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException("В сертификате указан неверный алгоритм");
        }
    }

    public static Pair<X509Certificate, PrivateKey> readPKCS12Certificate(String certPath, char[] password) {
        try{
            Security.addProvider(new BouncyCastleProvider());
            KeyStore p12 = KeyStore.getInstance("PKCS12", "BC");
            p12.load(new FileInputStream(certPath), password);
            Enumeration<String> e = p12.aliases();
            String alias = e.nextElement();
            X509Certificate certificate = (X509Certificate) p12.getCertificate(alias);
            if (certificate == null) {
                throw new CertificateException("Невозможно прочитать сертификат");
            }
            PrivateKey privateKey = (PrivateKey) p12.getKey(alias, password);
            if (privateKey == null) {
                throw new CertificateException("Невозможно получить ключ из сертификата");
            }
            return new Pair<>(certificate, privateKey);
        } catch (FileNotFoundException e) {
            throw new CertificateException("Файл сертификата не найден");
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException("В сертификате указан неверный алгоритм");
        } catch (IOException e){
            throw new PasswordException("Неверный пароль");
        } catch (java.security.cert.CertificateException | KeyStoreException | NoSuchProviderException | UnrecoverableKeyException e) {
            throw new CertificateException("Проблемы с сертификатом: " + e);
        }
    }

    public static PublicKey readPublicKeyFromPKCS12(String certPath, char[] pass) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            KeyStore p12 = KeyStore.getInstance("PKCS12", "BC");
            p12.load(new FileInputStream(certPath), pass);
            Enumeration<String> e = p12.aliases();
            String alias = e.nextElement();
            X509Certificate certificate = (X509Certificate) p12.getCertificate(alias);
            PublicKey pk = certificate.getPublicKey();
            return pk;
        } catch (FileNotFoundException e) {
            throw new CertificateException("Файл сертификата не найден");
        } catch (NoSuchAlgorithmException e1) {
            throw new CertificateException("В сертификате указан неверный алгоритм");
        } catch (IOException e){
            throw new PasswordException("Неверный пароль");
        } catch (java.security.cert.CertificateException | KeyStoreException | NoSuchProviderException e) {
            throw new CertificateException("Сертификат не соответствует требованиям");
        }
    }
}
