package ru.mirea.nir.provider;

import javafx.util.Pair;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import ru.mirea.nir.exceptions.SignatureException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Map;

public class SignUtils {
    public static void writeSignFile (Pair<String, String> signature, String path){
        try {
            File signFile = new File(path + "\\" + signature.getKey() + ".sig");
            PemWriter writer = new PEMWriter(new FileWriter(signFile));
            PemObject obj = new PemObject("DS DATA", signature.getValue().getBytes());
            writer.writeObject(obj);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new SignatureException("Файл подписи не найден");
        }
    }

    public static String readSignFile(File signFile){
        try {
            if (!signFile.getAbsolutePath().contains(".sig")) {
                throw new SignatureException("Файл подписи имеет неверное расширение");
            }
            FileInputStream fin = new FileInputStream(signFile);

            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            reader.close();

            String input = sb.toString();

            input = input.replace("-----BEGIN DS DATA-----", "");
            input = input.replace("-----END DS DATA-----", "");
            return new String(Base64.getDecoder().decode(input));
        } catch (FileNotFoundException e){
            throw new SignatureException("Файл подписи не найден");
        } catch (IOException e) {
            throw new SignatureException(e);
        }
    }
}
