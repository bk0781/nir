package ru.mirea.nir.provider;

import javafx.util.Pair;
import org.bouncycastle.operator.OperatorCreationException;

import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class GostCert {

    public enum GostVersion {
        Gost2012_256("ECGOST3410-2012", "Tc26-Gost-3410-12-256-paramSetA", "GOST3411WITHGOST3410-2012-256"),
        Gost2012_512("ECGOST3410-2012", "Tc26-Gost-3410-12-512-paramSetA", "GOST3411WITHGOST3410-2012-512");

        private final String algorithm;
        private final String params;
        private final String signatureAlgorithm;

        GostVersion(String algorithm, String params, String signatureAlgorithm) {
            this.algorithm = algorithm;
            this.params = params;
            this.signatureAlgorithm = signatureAlgorithm;
        }
    }

    public static KeyPair generateKeyPair (String alias, GostVersion version){
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            KeyPairGenerator keygen = KeyPairGenerator.getInstance(version.algorithm, "BC");
            keygen.initialize(new ECGenParameterSpec(version.params));

            return keygen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            System.out.printf("Algorithm %s is not found. %s\n", version.algorithm, e.getMessage());
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            System.out.printf("Initialization parameter %s is not found for algorithm %s. %s\n", version.params, version.algorithm, e.getMessage());
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            System.out.printf("Crypto provider BC is not found. %s\n", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static Pair<X509Certificate, PrivateKey> generateSelfSignedCertificate(String alias, GostVersion version) {
        KeyPair keyPair = generateKeyPair(alias, version);
        return generateSelfSignedCertificate(keyPair, alias, version);
    }

    public static Pair<X509Certificate, PrivateKey> generateSelfSignedCertificate(KeyPair keyPair, String alias, GostVersion version) {
        try {
            org.bouncycastle.asn1.x500.X500Name subject = new org.bouncycastle.asn1.x500.X500Name("CN=" + alias);
            org.bouncycastle.asn1.x500.X500Name issuer = subject;
            BigInteger serial = BigInteger.ONE;
            Date notBefore = new Date();
            Date notAfter = new Date(notBefore.getTime() + TimeUnit.DAYS.toMillis(365));

            org.bouncycastle.cert.X509v3CertificateBuilder certificateBuilder = new org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder(
                    issuer, serial,
                    notBefore, notAfter,
                    subject, keyPair.getPublic()
            );
            org.bouncycastle.cert.X509CertificateHolder certificateHolder = certificateBuilder.build(
                    new org.bouncycastle.operator.jcajce.JcaContentSignerBuilder(version.signatureAlgorithm)
                            .build(keyPair.getPrivate())
            );
            org.bouncycastle.cert.jcajce.JcaX509CertificateConverter certificateConverter = new org.bouncycastle.cert.jcajce.JcaX509CertificateConverter();

            X509Certificate certificate = certificateConverter.getCertificate(certificateHolder);
            PrivateKey privateKey = keyPair.getPrivate();

            return new Pair<>(certificate, privateKey);
        } catch (CertificateException | OperatorCreationException e) {
            throw new ru.mirea.nir.exceptions.CertificateException("Невозможно создать сертификат: " + e.getMessage());
        } finally {
            Security.removeProvider("BC");
        }
    }
}
