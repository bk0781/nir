package ru.mirea.nir.ds;

import javafx.util.Pair;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECPoint;
import ru.mirea.nir.exceptions.CertificateException;
import ru.mirea.nir.exceptions.SignatureException;
import ru.mirea.nir.hash.Gost3411;

import java.io.File;
import java.math.BigInteger;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECFieldFp;
import java.util.Map;

public class Gost3410 {
    private static ECPoint g;
    private static BigInteger q;
    private static ECFieldElement privateKey;
    private static ECPoint publicKey;
    private static boolean is512;

    private static void parsePublicKey(PublicKey pubKey) {
        try{
            ECPublicKey ECPubKey = (ECPublicKey) pubKey;
            is512 = ECPubKey.getParams().getCurve().getField().getFieldSize() == 512;
            BigInteger p = ((ECFieldFp)ECPubKey.getParams().getCurve().getField()).getP();
            ECCurve curve = new ECCurve.Fp(p, ECPubKey.getParams().getCurve().getA(), ECPubKey.getParams().getCurve().getB());

            ECFieldElement gX = new ECFieldElement.Fp(p, ECPubKey.getParams().getGenerator().getAffineX());
            ECFieldElement gY = new ECFieldElement.Fp(p, ECPubKey.getParams().getGenerator().getAffineY());

            g = new ECPoint.Fp(curve, gX, gY);
            q = ECPubKey.getParams().getOrder();

            ECFieldElement publicKeyX = new ECFieldElement.Fp(p, ECPubKey.getW().getAffineX());
            ECFieldElement publicKeyY = new ECFieldElement.Fp(p, ECPubKey.getW().getAffineY());

            publicKey = new ECPoint.Fp(curve, publicKeyX, publicKeyY);
        } catch (Exception e) {
            throw new CertificateException("Неверный сертификат");
        }
    }

    private static void parsePrivateKey(PrivateKey prKey) {
            try{
                ECPrivateKey ECPrKey = (ECPrivateKey) prKey;
                is512 = ECPrKey.getParams().getCurve().getField().getFieldSize() == 512;
                BigInteger p = ((ECFieldFp)ECPrKey.getParams().getCurve().getField()).getP();
                ECCurve curve = new ECCurve.Fp(p, ECPrKey.getParams().getCurve().getA(), ECPrKey.getParams().getCurve().getB());

                ECFieldElement gX = new ECFieldElement.Fp(p, ECPrKey.getParams().getGenerator().getAffineX());
                ECFieldElement gY = new ECFieldElement.Fp(p, ECPrKey.getParams().getGenerator().getAffineY());

                g = new ECPoint.Fp(curve, gX, gY);
                q = ECPrKey.getParams().getOrder();

                privateKey = new ECFieldElement.Fp(q, ECPrKey.getS());
        } catch (Exception e) {
            throw new CertificateException("Неверный сертификат");
        }
    }

    public static Pair<String, String> sign (File file, PrivateKey prKey){
        parsePrivateKey(prKey);

        ECFieldElement e = hash(file);
        ECFieldElement r;
        ECPoint c;
        ECFieldElement s;
        BigInteger k = rndK();

        do {
            do {
                c = g.multiply(k);
                r = new ECFieldElement.Fp(q, c.getX().toBigInteger());
            }
            while (r.toBigInteger().compareTo(BigInteger.ZERO) == 0);
            s = (r.multiply(privateKey))
                    .add(e.multiply(new ECFieldElement.Fp(q, k)));
        }
        while (s.toBigInteger().compareTo(BigInteger.ZERO) == 0);
        String fileName = file.getName();
        String r_ = "r_".concat(r.toBigInteger().toString(16));
        String s_ = "s_".concat(s.toBigInteger().toString(16));
        return new Pair<>(fileName, r_.concat(s_));
    }
    
    public static boolean checkSign (File file, String sign, PublicKey pubKey) {
        parsePublicKey(pubKey);
        BigInteger r_ = new BigInteger(sign.substring(sign.indexOf("r_") + 2, sign.indexOf("s_")), 16);
        BigInteger s_ = new BigInteger(sign.substring(sign.indexOf("s_") + 2), 16);
        if (r_.compareTo(BigInteger.ZERO) - r_.compareTo(q) + s_.compareTo(BigInteger.ZERO) - s_.compareTo(q) != 4) {
            throw new SignatureException("Неверные параметры подписи или сертификата");
        }


        ECFieldElement r = new ECFieldElement.Fp(q, r_);
        ECFieldElement s = new ECFieldElement.Fp(q, s_);
        ECFieldElement e = hash(file);

        ECFieldElement v = e.invert();

        ECFieldElement z1 = s.multiply(v);
        ECFieldElement z2 = r.negate().multiply(v);

        ECPoint c = g.multiply(z1.toBigInteger())
                .add(publicKey.multiply(z2.toBigInteger()));

        return c.getX().toBigInteger().mod(q).compareTo(r_) == 0;
    }

    private static ECFieldElement hash (File file) {
        BigInteger e;
        if (is512) {
            e = new BigInteger(new Gost3411(512).hash(file), 16);
        } else {
            e = new BigInteger(new Gost3411(256).hash(file), 16);
        }
        if (e.mod(q).compareTo(BigInteger.ZERO) == 0) {
            return new ECFieldElement.Fp(q, BigInteger.ONE);
        }
        return new ECFieldElement.Fp(q, e);
    }

    private static BigInteger rndK(){
        BigInteger k;
        do {
            k = new BigInteger(q.bitLength(), new SecureRandom());
        } while (k.compareTo(q) >= 0);
        return k;
    }
}
