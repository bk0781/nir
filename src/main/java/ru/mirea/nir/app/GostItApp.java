package ru.mirea.nir.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class GostItApp extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            URL xmlUrl = getClass().getResource("/fxml/main.fxml");
            loader.setLocation(xmlUrl);
            Parent root = loader.load();

            primaryStage.setScene(new Scene(root));
            primaryStage.setTitle("Gost It!");
            primaryStage.getIcons().add(new Image("/Gost It! logo.png"));
            primaryStage.setResizable(false);
            primaryStage.setWidth(460);
            primaryStage.setHeight(550);
            primaryStage.show();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);

            alert.setTitle("Exception Dialog");
            alert.setHeaderText("An error occurred:");

            String content = "Error: ";
            if (null != e) {
                content += e.toString() + "\n\n";
            }

            alert.setContentText(content);
        }
    }


    public static void main (String[] args){
        launch(args);
    }
}
