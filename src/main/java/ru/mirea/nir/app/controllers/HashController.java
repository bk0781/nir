package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class HashController extends GostItController{
    private final String[] modes = {"Вычислить хэш", "Сравнить с файлом", "Сравнить со значением"};
    private String modeState;

    @FXML
    private ChoiceBox modeChoiceBox;
    @FXML
    private AnchorPane secondaryAnchor;

    @FXML
    private void modeChoiceBoxAction() {
        String tmp = (String) modeChoiceBox.getValue();

        if (!modeState.equals(tmp)){
            modeState = tmp;

            if (modeState.equals(modes[0])) {
                loadCalcHashAnchor();
            } else if (modeState.equals(modes[1])) {
                loadCompFilesHashAnchor();
            } else if (modeState.equals(modes[2])) {
                loadCompValueHashAnchor();
            }
        }
    }

    void initChoosers(){
        modeChoiceBox.getItems().setAll(modes);
        modeState = modes[0];
        loadCalcHashAnchor();
    }

    private void loadCalcHashAnchor() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/hashAnchorsFXML/calcHash.fxml"));
            AnchorPane pane = loader.load();
            secondaryAnchor.getChildren().setAll(pane);
        } catch (IOException e){
            errorMessage(e);
        }
    }

    private void loadCompFilesHashAnchor (){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/hashAnchorsFXML/compFilesHash.fxml"));
            AnchorPane pane = loader.load();
            secondaryAnchor.getChildren().setAll(pane);
        } catch (IOException e){
            errorMessage(e);
        }
    }

    private void loadCompValueHashAnchor() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/hashAnchorsFXML/compValueHash.fxml"));
            AnchorPane pane = loader.load();
            secondaryAnchor.getChildren().setAll(pane);
        } catch (IOException e){
            errorMessage(e);
        }
    }
}
