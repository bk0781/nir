package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.util.Pair;
import ru.mirea.nir.exceptions.CertificateException;
import ru.mirea.nir.exceptions.PasswordException;
import ru.mirea.nir.provider.CertUtils;
import ru.mirea.nir.provider.GostCert;

import java.io.File;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Map;

public class CertController extends GostItController{
    private String certPathString;
    private String bitDepth = "512";
    private String pubKeyState = "Да";

    @FXML
    private ChoiceBox bitDepthChoiceBox;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ChoiceBox publicKeyChoiceBox;
    @FXML
    private Label certPath;
    @FXML
    private Button exportCertButton;

    @FXML
    private void chooseCertPath() {
        DirectoryChooser dc = new DirectoryChooser();
        File selectedDirectory = dc.showDialog(null);

        if (selectedDirectory != null) {
            enableExportCertButton();
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label");
            certPathString = selectedDirectory.getAbsolutePath();
            certPath.setText("Сохранить в: " + certPathString);
        }
    }

    @FXML
    private void bitDepthChoiceBoxAction() {
        String tmp = (String) bitDepthChoiceBox.getValue();

        if (!bitDepth.equals(tmp)) {
            enableExportCertButton();
            bitDepth = tmp;
        }
    }

    @FXML
    private void publicKeyChoiceBoxAction() {
        String tmp = (String) publicKeyChoiceBox.getValue();

        if (!pubKeyState.equals(tmp)){
            enableExportCertButton();
            pubKeyState = tmp;
        }
    }

    @FXML
    private void passwordFieldAction() {
        if (passwordField.getText() != null) {
            enableExportCertButton();
            passwordField.getStyleClass().clear();
            passwordField.getStyleClass().add("password-field");
        }
    }

    @FXML
    private void exportCert() {
        try {
            disableExportCertButton();
            if (certPathString == null){
                throw new CertificateException("Путь сохранения сертификата не выбран");
            }

            Pair<X509Certificate, PrivateKey> certificateAndPrivateKey;
            String ALIAS = "Gost It!";

            if (bitDepth.equals("512")) {
                certificateAndPrivateKey = GostCert.generateSelfSignedCertificate(ALIAS, GostCert.GostVersion.Gost2012_512);
            } else {
                certificateAndPrivateKey = GostCert.generateSelfSignedCertificate(ALIAS, GostCert.GostVersion.Gost2012_256);
            }

            char[] pass;
            if (passwordField.getText().length() != 0){
                pass = passwordField.getText().toCharArray();
            } else {
                pass = "".toCharArray();
            }

            CertUtils.writePKCS12Certificate(certificateAndPrivateKey.getKey(), certificateAndPrivateKey.getValue(),
                    certPathString, pass);

            if (pubKeyState.equals("Да")) {
                CertUtils.writeX509Certificate(certificateAndPrivateKey.getKey(), certPathString);
            }

            certExportedMessage();

        } catch (CertificateException e){
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (PasswordException e) {
            passwordField.getStyleClass().clear();
            passwordField.getStyleClass().add("password-field_error");
            errorMessage(e.getMessage());
        }
    }

    private void disableExportCertButton () {
        exportCertButton.setDisable(true);
    }

    private void enableExportCertButton () {
        exportCertButton.setDisable(false);
    }

    void initChoosers(){
        bitDepthChoiceBox.getItems().addAll("256", "512");
        publicKeyChoiceBox.getItems().addAll("Да", "Нет");
    }
}
