package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

public class MainController extends GostItController{
    @FXML
    private void openSign() {
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/fxml/sign.fxml"));
            rootPane.getChildren().setAll(pane);
        } catch (Exception e){
            errorMessage(e);
        }
    }

    @FXML
    private void openCheck() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/checkSign.fxml"));
            AnchorPane pane = loader.load();
            rootPane.getChildren().setAll(pane);

        } catch (Exception e){
            errorMessage(e);
        }
    }

    @FXML
    private void openCert() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/cert.fxml"));
            AnchorPane pane = loader.load();
            rootPane.getChildren().setAll(pane);

            CertController controller = loader.getController();
            controller.initChoosers();
        } catch (Exception e){
            errorMessage(e);
        }
    }

    @FXML
    private void openHash() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/hash.fxml"));
            AnchorPane pane = loader.load();
            rootPane.getChildren().setAll(pane);

            HashController controller = loader.getController();
            controller.initChoosers();
        } catch (Exception e){
            errorMessage(e);
        }
    }

    @FXML
    private void openAbout() {
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/fxml/about.fxml"));
            rootPane.getChildren().setAll(pane);
        } catch (Exception e){
            errorMessage(e);
        }
    }
}
