package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Pair;
import ru.mirea.nir.ds.Gost3410;
import ru.mirea.nir.provider.CertUtils;
import ru.mirea.nir.provider.SignUtils;
import ru.mirea.nir.exceptions.*;

import java.io.File;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.util.Map;

public class SignController extends GostItController{
    private File file;
    private String cert;
    private String signPathString;

    @FXML
    private Label filePath;
    @FXML
    private Label certPath;
    @FXML
    private Label signPath;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button signButton;

    @FXML
    private void chooseFile() {
        FileChooser fc = new FileChooser();
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            file = tmp;
            enableSignButton();
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label");
            filePath.setText("Документ: " + tmp.getName());
        }
    }

    @FXML
    private void chooseCert() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("PKCS12 сертификаты (*.pfx)", "*.pfx"));
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            cert = tmp.getAbsolutePath();
            enableSignButton();
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label");
            certPath.setText("Сертификат: " + tmp.getName());
        }
    }

    @FXML
    private void chooseSignPath() {
        DirectoryChooser dc = new DirectoryChooser();
        File selectedDirectory = dc.showDialog(null);

        if (selectedDirectory != null){
            enableSignButton();
            signPath.getStyleClass().clear();
            signPath.getStyleClass().add("path_label");
            signPathString = selectedDirectory.getAbsolutePath();
            signPath.setText("Сохранить в: " + signPathString);
        }
    }

    @FXML
    private void passwordFieldAction() {
        if (passwordField.getText() != null) {
            enableSignButton();
            passwordField.getStyleClass().clear();
            passwordField.getStyleClass().add("password-field");
        }
    }

    @FXML
    private void sign() {
        try {
            disableSignButton();
            if (file == null){
                throw new FileException("Документ не выбран");
            } else if (cert == null){
                throw new CertificateException("Сертификат не выбран");
            } else if (signPathString == null){
                throw new SignatureException("Путь сохранения подписи не выбран");
            }

            char[] pass;
            if (passwordField.getText().length() != 0){
                pass = passwordField.getText().toCharArray();
            } else {
                pass = "".toCharArray();
            }

            Pair<X509Certificate, PrivateKey> certRead = CertUtils.readPKCS12Certificate(cert, pass);
            Pair<String, String> sign = Gost3410.sign(file, certRead.getValue());
            SignUtils.writeSignFile(sign, signPathString);

            signedMessage();

        } catch (FileException e){
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (CertificateException e){
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (PasswordException e) {
            passwordField.getStyleClass().clear();
            passwordField.getStyleClass().add("password-field_error");
            errorMessage(e.getMessage());
        } catch (SignatureException e) {
            signPath.getStyleClass().clear();
            signPath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (Exception e) {
            errorMessage(e);
        }
    }

    private void disableSignButton() {
        signButton.setDisable(true);
    }

    private void enableSignButton() {
        signButton.setDisable(false);
    }
}
