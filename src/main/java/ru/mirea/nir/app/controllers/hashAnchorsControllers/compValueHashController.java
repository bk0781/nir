package ru.mirea.nir.app.controllers.hashAnchorsControllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import ru.mirea.nir.app.controllers.GostItController;
import ru.mirea.nir.exceptions.FileException;
import ru.mirea.nir.exceptions.HashException;
import ru.mirea.nir.hash.Gost3411;

import java.io.File;
import java.math.BigInteger;

public class compValueHashController extends GostItController{
    private File file;
    private String hashValue;

    @FXML
    private Label filePath;
    @FXML
    private TextArea hashValueArea;
    @FXML
    private Button hashButton;

    @FXML
    private void chooseFile() {
        FileChooser fc = new FileChooser();
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            file = tmp;

            enableHashButton();
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label");
            filePath.setText("Документ: " + tmp.getName());
        }
    }

    @FXML
    private void compValue() {
        try{
            if (file == null) {
                disableHashButton();
                throw new FileException("Документ не выбран");
            } if (hashValueArea.getText().isEmpty()) {
                throw new Exception("Введите значение хэша");
            }

            hashValue = new BigInteger(hashValueArea.getText().toLowerCase(), 16).toString(16);

            String fileHash = new Gost3411(hashValue.length() * 4).hash(file);

            if (fileHash.equals(hashValue)) {
                hashMessage("Значения идентичны", false);
            } else {
                hashMessage("Значния отличаются", true);
            }

        } catch (FileException e) {
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (NumberFormatException e) {
            errorMessage("Введенное значение содержит недопустимые символы");
        } catch (HashException e) {
            errorMessage(e.getMessage());
        } catch (Exception e) {
            errorMessage(e);
        }
    }

    private void disableHashButton() {
        hashButton.setDisable(true);
    }

    private void enableHashButton() {
        hashButton.setDisable(false);
    }
}
