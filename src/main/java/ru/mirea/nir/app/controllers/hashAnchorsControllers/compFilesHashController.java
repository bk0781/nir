package ru.mirea.nir.app.controllers.hashAnchorsControllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import ru.mirea.nir.app.controllers.GostItController;
import ru.mirea.nir.exceptions.FileException;
import ru.mirea.nir.hash.Gost3411;

import java.io.File;

public class compFilesHashController extends GostItController{
    private File firstFile;
    private File secondFile;

    @FXML
    private Label firstFilePath;
    @FXML
    private Label secondFilePath;
    @FXML
    private Button hashButton;

    @FXML
    private void chooseFirstFile() {
        FileChooser fc = new FileChooser();
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            firstFile = tmp;

            enableHashButton();
            firstFilePath.getStyleClass().clear();
            firstFilePath.getStyleClass().add("path_label");
            firstFilePath.setText("Документ: " + tmp.getName());
        }
    }

    @FXML
    private void chooseSecondFile() {
        FileChooser fc = new FileChooser();
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            secondFile = tmp;

            enableHashButton();
            secondFilePath.getStyleClass().clear();
            secondFilePath.getStyleClass().add("path_label");
            secondFilePath.setText("Документ: " + tmp.getName());
        }
    }


    @FXML
    private void compareFiles() {
        try{
            disableHashButton();
            if (firstFile == null) {
                firstFilePath.getStyleClass().clear();
                firstFilePath.getStyleClass().add("path_label_error");
                throw new FileException("Документ не выбран");
            } if (secondFile == null) {
                secondFilePath.getStyleClass().clear();
                secondFilePath.getStyleClass().add("path_label_error");
                throw new FileException("Документ не выбран");
            }
            String hashFile1 = new Gost3411(512).hash(firstFile);
            String hashFile2 = new Gost3411(512).hash(secondFile);
            if (hashFile1.equals(hashFile2)) {
                hashMessage("Файлы идентичны", false);
            } else {
                hashMessage("Файлы отличаются", true);
            }
        } catch (Exception e){
            errorMessage(e);
        } catch (FileException e) {
            errorMessage(e.getMessage());
        }
        }

    private void disableHashButton() {
        hashButton.setDisable(true);
    }

    private void enableHashButton() {
        hashButton.setDisable(false);
    }
}
