package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class GostItController{
    @FXML
    public AnchorPane rootPane;

    public void errorMessage(String msg) {
        showMessage("Ошибка", msg, true);
    }

    public void errorMessage(Exception e) {
        String msg;
        if (e.getMessage() == null) {
            msg = e.toString();
            e.printStackTrace();
        } else {
            msg = e.getMessage();
        }
        showMessage("Ошибка", msg, true);
    }

    void signedMessage(){
        showMessage("Результат", "Документ подписан успешно", false);
    }

    void signCorrectMessage() {
        showMessage("Результат", "Подпись действительна", false);
    }

    void signIncorrectMessage() {
        showMessage("Результат", "Подпись недействительна", true);
    }

    void certExportedMessage() {
        showMessage("Результат", "Сертификат успешно экспортирован", false);
    }

    public void hashMessage(String message, boolean isWarning) {
        showMessage("Результат", message, isWarning);
    }

    private void showMessage(String messageHeaderString, String messageTextString, Boolean isWarning) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/message.fxml"));
            root = loader.load();

            Stage stage = new Stage();
            stage.setTitle("Gost It! - Уведомление");
            stage.setScene(new Scene(root));
            stage.initStyle(StageStyle.UTILITY);
            stage.setResizable(false);

            MessageController controller = loader.getController();
            controller.initMessage(messageHeaderString, messageTextString, isWarning);
            stage.show();
        } catch (Exception e) {
            System.out.println();
        }
    }

    @FXML
    private void backToMain() {
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
            rootPane.getChildren().setAll(pane);
        } catch (Exception e){
            errorMessage(e);
        }
    }
}
