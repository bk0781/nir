package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.FileChooser;
import ru.mirea.nir.ds.Gost3410;
import ru.mirea.nir.exceptions.*;
import ru.mirea.nir.provider.CertUtils;
import ru.mirea.nir.provider.SignUtils;

import java.io.File;
import java.security.PublicKey;

public class CheckSignController extends GostItController{
    private File file;
    private String cert;
    private File sign;

    @FXML
    private Label filePath;
    @FXML
    private Label signPath;
    @FXML
    private Label certPath;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button checkSignButton;

    @FXML
    private void chooseFile() {
        FileChooser fc = new FileChooser();
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            file = tmp;
            enableCheckSignButton();
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label");
            filePath.setText("Документ: " + tmp.getName());
        }
    }

    @FXML
    private void chooseCert() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("PKCS12 сертификаты (*.pfx)", "*.pfx"));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("PEM сертификаты (*.crt)", "*.crt"));
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            cert = tmp.getAbsolutePath();

            enableCheckSignButton();
            if (cert.contains(".crt")) {
                passwordField.setDisable(true);
            } else {
                passwordField.setDisable(false);
            }

            enableCheckSignButton();
            signPath.getStyleClass().clear();
            signPath.getStyleClass().add("path_label");
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label");

            certPath.setText("Сертификат: " + tmp.getName());
        }
    }

    @FXML
    private void chooseSign() {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Файлы ЭЦП (*.sig)", "*.sig"));

        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            sign = tmp;
            enableCheckSignButton();
            signPath.getStyleClass().clear();
            signPath.getStyleClass().add("path_label");
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label");

            signPath.setText("Подпись: " + tmp.getName());
        }
    }

    @FXML
    private void passwordFieldAction() {
        if (passwordField.getText() != null) {
            enableCheckSignButton();
            passwordField.getStyleClass().clear();
            passwordField.getStyleClass().add("password-field");
        }
    }

    @FXML
    private void checkSign() {
        try {
            disableCheckSignButton();
            if (file == null){
                throw new FileException("Документ не выбран");
            } else if (sign == null){
                throw new SignatureException("Файл подписи не выбран");
            } else if (cert == null){
               throw new CertificateException("Сертификат не выбран");
            }

            char[] pass;
            if (passwordField.getText().length() != 0){
                pass = passwordField.getText().toCharArray();
            } else {
                pass = "".toCharArray();
            }

            PublicKey pubKey;
            if (cert.contains(".crt")) {
                pubKey = CertUtils.readPublicKeyPEM(cert);
            } else {
                pubKey = CertUtils.readPublicKeyFromPKCS12(cert, pass);
            }
            boolean isCorrectSign = Gost3410.checkSign(file, SignUtils.readSignFile(sign), pubKey);

            if (isCorrectSign) {
                signCorrectMessage();
            } else {
                signIncorrectMessage();
            }

        } catch (FileException e){
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (CertificateException e){
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (SignatureException e) {
            signPath.getStyleClass().clear();
            signPath.getStyleClass().add("path_label_error");
            certPath.getStyleClass().clear();
            certPath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        } catch (PasswordException e) {
            passwordField.getStyleClass().clear();
            passwordField.getStyleClass().add("password-field_error");
            errorMessage(e.getMessage());
        } catch (Exception e) {
            errorMessage(e);
        }
    }

    private void enableCheckSignButton() {
        checkSignButton.setDisable(false);
    }

    private void disableCheckSignButton() {
        checkSignButton.setDisable(true);
    }
}
