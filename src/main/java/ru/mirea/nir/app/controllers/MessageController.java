package ru.mirea.nir.app.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class MessageController{
    @FXML
    private Label messageHeader;
    @FXML
    private Label messageText;
    @FXML
    private Button okButton;

    void initMessage(String messageHeaderString, String messageTextString, Boolean isWarning) {
        messageHeader.setText(messageHeaderString);
        messageText.setText(messageTextString);

        if (isWarning) {
            messageHeader.getStyleClass().add("message_header_warning");
            okButton.getStyleClass().add("message_button_warning");
        } else {
            messageHeader.getStyleClass().add("message_header_normal");
            okButton.getStyleClass().add("message_button_normal");
        }
    }

    @FXML
    private void closeWindow() {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }
}
