package ru.mirea.nir.app.controllers.hashAnchorsControllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import ru.mirea.nir.app.controllers.GostItController;
import ru.mirea.nir.exceptions.FileException;
import ru.mirea.nir.hash.Gost3411;

import java.io.File;

public class calcHashController extends GostItController{
    private File file;

    @FXML
    private Label filePath;
    @FXML
    private TextArea result;
    @FXML
    private Button hashButton;


    @FXML
    private void chooseFile() {
        FileChooser fc = new FileChooser();
        File tmp = fc.showOpenDialog(null);

        if (tmp != null){
            file = tmp;

            enableHashButton();
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label");
            filePath.setText("Документ: " + tmp.getName());
        }
    }

    @FXML
    private void calcHash() {
        try{
            disableHashButton();
            if (file == null) {
                throw new FileException("Документ не выбран");
            }

            String hash256 = new Gost3411(256).hash(file);

            String hash512 = new Gost3411(512).hash(file);

            result.setText("ГОСТ Р34.10-2012 256 бит: " + hash256 + "\n\nГОСТ Р34.10-2012 512 бит: " + hash512);

        } catch (FileException e) {
            filePath.getStyleClass().clear();
            filePath.getStyleClass().add("path_label_error");
            errorMessage(e.getMessage());
        }
    }

    private void disableHashButton() {
        hashButton.setDisable(true);
    }

    private void enableHashButton() {
        hashButton.setDisable(false);
    }
}
