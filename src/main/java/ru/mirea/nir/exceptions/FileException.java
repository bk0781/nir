package ru.mirea.nir.exceptions;

public class FileException extends Error {
    public  FileException (String message) {
        super(message);
    }

    public  FileException (Exception e) {
        super("Проблемы с документом: " + e.getMessage());
    }
}
