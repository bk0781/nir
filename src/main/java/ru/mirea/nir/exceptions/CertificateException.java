package ru.mirea.nir.exceptions;

public class CertificateException extends Error {
    public CertificateException(String message) {
        super(message);
    }

    public CertificateException(Exception e) {
        super("Проблемы с сертификатом: " + e.getMessage());
    }
}
