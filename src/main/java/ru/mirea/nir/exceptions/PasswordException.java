package ru.mirea.nir.exceptions;

public class PasswordException extends Error {
    public PasswordException (String message){
        super(message);
    }
}
