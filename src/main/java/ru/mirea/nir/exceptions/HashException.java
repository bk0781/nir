package ru.mirea.nir.exceptions;

public class HashException extends Error {
    public HashException(String message) {
        super(message);
    }

    public HashException(Exception e) {
        super("Проблемы с введенным значением: " + e.getMessage());
    }
}
