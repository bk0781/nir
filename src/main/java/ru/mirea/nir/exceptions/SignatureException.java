package ru.mirea.nir.exceptions;

public class SignatureException extends Error {
    public  SignatureException (String message) {
        super(message);
    }

    public  SignatureException (Exception e) {
        super("Проблемы с файлом подписи: " + e.getMessage());
    }
}
