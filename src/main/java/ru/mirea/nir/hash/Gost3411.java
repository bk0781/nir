package ru.mirea.nir.hash;

import ru.mirea.nir.exceptions.FileException;
import ru.mirea.nir.exceptions.HashException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Gost3411 {
    private int[] h;
    private int[] M;
    private int[] N;
    private int[] Sigma;
    private int[] m;
    private final int SIZE;
    private int l;
    private final int BUFF_SIZE = 4096;

    public Gost3411(int size){
        this.SIZE = size;
        int[] IV = new int[64];
        if (size == 256) {
            for (int i = 0; i < IV.length; i++) {
                IV[i] = 0x01;
            }
        } else if (size != 512){
            throw new HashException("Длина хэша должна быть равна 256 или 512 бит");
        }
        this.h = new int[64];
        System.arraycopy(IV, 0, this.h, 0, 64);
        this.N = new int[64];

        this.Sigma = new int[64];
        this.m = new int[64];
    }

    private int[] S(int[] a) {
        int[] result = new int[64];
        for (int i = 0; i < 64; i++) {
            result[i] = Args.PI[a[i]];
        }
        return result;
    }

    private int[] P(int[] a) {
        int[] result = new int[64];
        for (int i = 0; i < 64; i++) {
            result[i] = a[Args.TAU[i]];
        }
        return result;
    }

    private int[] L(int[] a) {
        int[] result = new int[64];
        for (int i = 0; i < 8; i++) {
            int[] v = new int[8];
            for (int k = 0; k < 8; k++) {
                for (int j = 0; j < 8; j++) {
                    if ((a[i * 8 + k] & (1 << (7 - j))) != 0) {
                        v = xor(v, Args.A[k * 8 + j]);
                    }
                }
            }
            System.arraycopy(v, 0, result, i * 8, 8);
        }
        return result;
    }

    private int[] xor(int[] a, int[] b) {
        int[] result = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            result[i] = a[i] ^ b[i];
        }
        return result;
    }

    private int[] gN(int[] Nn, int[] h, int[] m) {
        int[] K = LPS(xor(h, Nn));
        int[] e = E(K, m);
        return xor(e, xor(h, m));
    }

    private int[] LPS(int[] state) {
        return L(P(S(state)));
    }

    private int[] E(int[] K, int[] m) {
        int[] result = xor(K, m);
        int[] Ki = new int[64];
        System.arraycopy(K, 0, Ki, 0, 64);
        for (int i = 0; i < 12; i++) {
            result = LPS(result);
            Ki = ks(Ki, i);
            result = xor(result, Ki);
        }
        return result;
    }

    private int[] ks(int[] k, int i) {
        return LPS(xor(k, Args.C[i]));
    }

    private void update(int[] input) {

        this.M = new int[input.length];
        System.arraycopy(input, 0, M, 0, input.length);

        l = input.length;
        while (l >= 64) {
            System.arraycopy(M, l - 64, m, 0, 64);
            h = gN(N, h, m);
            N = add(N, Args.bv512);
            Sigma = add(Sigma, m);
            l -= 64;
        }
    }

    private int[] calculate (){
        for (int i = 0; i < 63 - l; i++) {
            m[i] = 0;
        }
        m[63 - l] = 0x01;
        if (l > 0) {
            System.arraycopy(M, 0, m, 63 - l + 1, l);
        }

        h = gN(N, h, m);
        int[] bv = new int[64];
        bv[62] = (l * 8) >> 8;
        bv[63] = (l * 8) & 0xFF;
        N = add(N, bv);
        Sigma = add(Sigma, m);
        h = gN(Args.bv00, h, N);
        h = gN(Args.bv00, h, Sigma);
        return h;
    }

    private int[] convertToIntArray(byte[] buff){
        int[] arr = new int[buff.length];
        for (int i = 0; i < arr.length; i++){
            arr[i] = Byte.toUnsignedInt(buff[i]);
        }
        return arr;
    }

    public String hash (File file) {
        try{
            FileInputStream fis = new FileInputStream(file);
            FileChannel channel = fis.getChannel();
            long fileSize = file.length();
            ByteBuffer buff;
            if (BUFF_SIZE > fileSize) {
                buff = ByteBuffer.allocate((int) fileSize);//todo чтение с конца
            } else {
                buff = ByteBuffer.allocate(BUFF_SIZE);
            }
            while(channel.read(buff) != -1) {
                buff.flip();
                update(convertToIntArray(buff.array()));
                buff.clear();
                fileSize -= BUFF_SIZE;
                if (fileSize < 1) {
                    break;
                }
                if (BUFF_SIZE > fileSize) {
                    buff = ByteBuffer.allocate((int) fileSize);
                }
            }
            int[] out = calculate();
            return resultToString(out, SIZE);
        } catch (IOException e) {
            throw new FileException(e);
        }
    }

    private int[] add(int[] a, int[] b) {
        int[] result = new int[a.length];
        int r = 0;
        for (int i = a.length - 1; i >= 0; i--) {
            result[i] = (a[i] + b[i] + r) & 255;
            r = ((a[i] + b[i]) >> 8) & 255;
        }
        return result;
    }

    private String resultToString (int[] result, int size){
        String out = "";
        for (int i = 0; i < size / 8; i++){
            if (result[i] < 16) {
                out = out.concat("0");
            }
            out = out.concat(Integer.toHexString(result[i]));
        }
        return out;
    }
}
